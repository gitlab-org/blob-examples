class Branch {
  constructor(name) {
    this.name = name;
    this.fullName = `full ${name}`;
  }
}

export default Branch;
